# Change Log

## [1.8.0] - 2016-05-09
### Added
- AnimalBikes_1.8.9(22feb16)
- baublesstuff-2.0.3
- intangible-1.8.9-0.0.25
- ZyinsHUD-(1.8.8)-v.1.4.8

### Updated
- EvilCraft-1.8.9-0.9.7
- CyclopsCore-1.8.9-0.5.6
- BloodMagic-1.8.9-2.0.0-20

### Disabled
- TimeHUD-4.0.1.7

## [1.7.0] - 2016-05-03
### Added
- CraftTweaker-1.8.8-3.0.2
- RebornCore-1.8.9-1.5.1.57-universal
- Slabcraft-V3.01 - MC1.8.9 Forge
- Slabify-1.8.9-1.2.1
- Staircraft-V3.01 - MC1.8.9 Forge
- Wallcraft-V3.01 - MC1.8.9 Forge
- hopperducts-mc1.8.8-1.4.6
- immcraft-1.1.2
- mcjtylib-1.8.9-1.8.1beta10
- missingPieces-1.8.9-1.4.0
- paintthis-0.0.2-1.8.9
- rftools-1.8.9-4.23beta40
- rftoolsdim-1.8.9-4.22beta31
- rpcraft-1.8.9-2.1.0

### Disabled
- MrCrayfishFurntiureModv3.6.1(1.8.9)

## [1.6.0] - 2016-05-01
### Added
- EnderCompass-1.8.9-1.2.2
- VanillaTeleporter-1.8.9-1.5.1
- generators-0.9.19.5-mc1.8.9
- bdlib-1.10.0.26-mc1.8.9

### Removed
- catwalks-0.1.1.jar
- tobymodxp-2.0.jar

## [1.5.0] - 2016-04-29
### Added
- Backlytra-1.8.9-0.0.3
- EnderRift-1.8.9-0.15.0
- LLOverlayReloaded-1.0.7-mc1.8.9
- ResourcefulCrops-1.8.9-1.3.0-44
- enderthing-1.8.9-0.5.1
- enderutilities-1.8.9-0.5.3
- netherportalfix-mc1.8.9-2.1.6

### Updated
- inventorypets-1.8.9-1.4.4

## [1.4.0] - 2016-04-28
### Added
- ActuallyAdditions-1.8.9-r26
- ColossalChests-1.8.9-1.3.3
- CyclopsCore-1.8.9-0.5.5
- EvilCraft-1.8.9-0.9.6
- RouterReborn-1.8.9-3.0.5.14b_universal
- SimpleGrinder-0.150.R189
- practicalities-1.8.9-2.0.0-b6
- quantumflux-1.8.9-2.0.1

### Updated
- EnderZoo-1.8.9-1.1.4.36
- catwalks-0.1.1
