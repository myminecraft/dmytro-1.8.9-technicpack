# Mods

- 1.8.9-chunkedgeindicator-1.0
- AbyssalCraft-1.8.9-1.9.1.1
- ActuallyAdditions-1.8.9-r26
- ArchitectureCraft-1.4.1-mc1.8.9
- Backlytra-1.8.9-0.0.3
- Bagginses-1.8.9-2.1.5b
- BaseMetals_1.8.9-1.8.0
- bdlib-1.10.0.26-mc1.8.9
- BetterAchievements-1.8.9-0.1.1
- BetterFps-1.2.0
- BloodMagic-1.8.9-2.0.0-19
- Bookshelf-API-1.8.9-2.3.7
- Botania-unofficial.r1.8-299
- buildcraft-7.2.5
- buildcraft-compat-7.2.0
- ChainCrafting-1.0.0_1.8.9
- Chameleon-1.8.9-0.2.1
- Chisel-MC1.8.9-0.0.2.6
- ClayBucket-1.8.9-1.2
- ColossalChests-1.8.9-1.3.3
- ComputerCraft1.79
- ComputerCraftEdu1.79
- Craftable Horse Armour and Saddle 1.2.1
- craftingtweaks-mc1.8.9-3.0.52
- CraftTweaker-1.8.8-3.0.2
- CyclopsCore-1.8.9-0.5.5
- denseores-1.8-alpha-2.0.0
- DungeonTactics-1.8.9-0.9.3
- ElectricAdvantage-1.1.2
- Elevator Mod-2.1
- EnderCompass-1.8.9-1.2.2
- EnderRift-1.8.9-0.15.0
- EnderTanks-1.8.9-1.2.1
- enderthing-1.8.9-0.5.1
- enderutilities-1.8.9-0.5.3
- EnderZoo-1.8.9-1.1.4.36
- EvilCraft-1.8.9-0.9.6
- extrautils2-1.8.9-alpha-0.0.1
- FastLeafDecay-1.8-1.6
- Floocraft-1.8.9-0.2
- Flowercraft-V3.02 - MC1.8.9 Forge
- Fluidity 4.0.0.4
- FoodDetails-1.8.9-1.0.0
- Furnus-1.8.9-1.73
- gemsplus-1.6.4-mc1.8.8
- generators-0.9.19.5-mc1.8.9
- Grapple Hooks-1.8.9-1.0.4
- HelpFixer-1.8.9-1.2.12
- hopperducts-mc1.8.8-1.4.6
- immcraft-1.1.2
- industrialcraft-2-2.3.222-ex18
- inventorypets-1.8.9-1.4.4
- inventorysorter-1.8.9-0.7.4+26
- InventoryTweaks-1.60-beta-30
- ironchest-1.8.9-6.0.125.770
- jei_1.8.9-2.28.16.184
- JEIAddons_1.8.9-0.12.0.57
- journeymap-1.8.8-5.1.3-unlimited
- JustEnoughResources-1.8.9-0.4.0
- LLOverlayReloaded-1.0.7-mc1.8.9
- LunatriusCore-1.8.9-1.1.2.32-universal
- malisiscore-1.8.9-2.1.3
- malisisdoors-1.8.9-3.1.3
- Mantle-1.8.9-0.9.1
- MCA-1.8.9-5.1.2-universal
- mcjtylib-1.8.9-1.8.1beta10
- MCMultiPart-1.0.8-universal
- MineMenu-1.8.9-1.3.0.B45-universal
- mineralogy_1.8.9-2.6.1
- missingPieces-1.8.9-1.4.0
- modnametooltip_1.8.8-1.0.0
- MorePlayerModels_1.8.9(27feb16)
- MrCrayfishFurntiureModv3.6.1(1.8.9).disabled
- Neat 1.1-2
- NeoTech-1.8.9-2.4.4
- NetherMetals-1.0.3
- netherportalfix-mc1.8.9-2.1.6
- NotEnoughKeys-1.8.9-3.0.0b1-dev-universal
- Packing Tape-0.3.5
- paintthis-0.0.2-1.8.9
- Pam's Clay Spawn 1.8.9a
- Pam's Get all the Seeds! 1.8.9a
- Pam's HarvestCraft 1.8.9d
- Papertazer Base Mod-1.1
- PowerAdvantage-1.5.4
- practicalities-1.8.9-2.0.0-b6
- ProgressiveAutomation-1.8.9-1.6.44
- ProjectE-1.8.9-PE1.1.4
- quantumflux-1.8.9-2.0.1
- RadixCore-1.8.9-2.1.0-universal
- RandomThings-MC1.8.9-3.6.6
- RealTimeClock-1.8.9-0.5
- RebornCore-1.8.9-1.5.1.57-universal
- Reliquary-1.8.9-1.3.1.296
- ResourcefulCrops-1.8.9-1.3.0-44
- rftools-1.8.9-4.23beta40
- rftoolsdim-1.8.9-4.22beta31
- RoguelikeDungeons-1.8.9-1.4.5
- RouterReborn-1.8.9-3.0.5.14b_universal
- rpcraft-1.8.9-2.1.0
- ShetiPhianCore-1.8.9-3.1.0
- SimpleGraves-1.8.9-1.0.0-6
- SimpleGrinder-0.150.R189
- simpleretrogen-1.8.9-2.2.2+3
- Slabcraft-V3.01 - MC1.8.9 Forge
- Slabify-1.8.9-1.2.1
- SpawnerCraft2-1.8.9-2.3
- Stackie-1.8.9-1.6.0.39-universal
- Staircraft-V3.01 - MC1.8.9 Forge
- SteamAdvantage-1.5.3
- StorageDrawers-1.8.9-2.4.4
- StorageNetwork-1.8.9-1.9.1
- TConstruct-1.8.9-2.2.1
- Thaumcraft-1.8.9-5.2.4
- ThaumicInfusion-1.8.9-4.83
- thetooltooltiptool-1.8.9-0.3.0-4-universal
- TimeHUD-4.0.1.7
- trashslot-mc1.8.9-2.0.7
- TRTweaks-1.8.9-1.0.0
- UncraftingGrinder-1.8.9-1.0.2
- VanillaTeleporter-1.8.9-1.5.1
- VTweaks-1.8.x-1.4.6
- Waila-1.6.0-B3_1.8.8
- WailaHarvestability-mc1.8.x-1.1.7
- Wallcraft-V3.01 - MC1.8.9 Forge
- Warpbook-1.8.9_2.0.43
- Wawla-1.8.9-1.1.4.171
- Zoooooom-1.8.9-0.1.2